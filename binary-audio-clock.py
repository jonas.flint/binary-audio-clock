
import sys
from datetime import datetime
import time
import pyttsx3


def setGermanLang(engine):
  voices = engine.getProperty('voices')
  for v in voices:
    if v.id == 'german':
       print('set German language')
       engine.setProperty('voice',v.id)
       break


def sayCurrentTime(engine, now):
  minute_str = str(now.minute) if now.minute > 0 else ''
  time_str = '{} Uhr {}'.format(now.hour, minute_str)

  print('saying time:', time_str)

  engine.setProperty('rate', 130)
  engine.say(time_str)
  engine.runAndWait()


def sayCurrentBinaryTime(engine, now):
  engine.setProperty('rate', 150)

  hour_binary_str = ' '.join('{0:b}'.format(now.hour)) + ' Uhr'
  print('saying binary hour:', hour_binary_str)
  engine.say(hour_binary_str)
  engine.runAndWait()

  minute_binary_str = ' '.join('{0:b}'.format(now.minute)) + ' Minuten'
  print('saying binary minute:', minute_binary_str)
  engine.say(minute_binary_str)
  engine.runAndWait()


def loop(driverNameVar, debugging=False):
  engine = pyttsx3.init(driverName=driverName)
  setGermanLang(engine) 

  last_minute = None
  last_hour = None
  n = 0

  while True:
    now = datetime.now()

    '''
    # check current hour
    if last_hour != now.hour or n == 0:
      sayCurrentTime(engine, now)
      last_hour = now.hour
    
    if last_minute == 30 and n > 1:
      sayCurrentTime(engine, now)
    '''
    
    if last_minute != now.minute:
      sayCurrentTime(engine, now)
      sayCurrentBinaryTime(engine, now)
      last_minute = now.minute
      print('time was:', now)

    n = n + 1
    time.sleep(1)

    if debugging and n > 2:
      break


if __name__ == '__main__':
  driverNameVar = sys.argv[1] if len(sys.argv) > 1 else None
  driverName = driverNameVar if driverNameVar in ['sapi5', 'nsss', 'espeak'] else None
  loop(driverName, False)

