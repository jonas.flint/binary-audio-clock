# Binary audio clock

This scripts tells you the current time in binary.

## Usage

Just run the script:
```shell
$ python3 binary-audio-clock.py
```

## Install

Install the ``pyttsx3`` and a driver (espeak for example).

```shell
$ sudo pip3 install pyttsx3
$ sudo apt install espeak
```

## Autostart

Add a `@reboot`-entry to your crontab.

```shell
$ sudo crontab -e
```

which should look like this

```
@reboot python3 /home/pi/binary-audio-clock/binary-audio-clock.py >> /home/pi/binary-audio-clock/log.txt
```
